# Instruction for developers

'People Clicker: Inception' application is developed via Corona SDK.

## Download and install development tools

- Install Corona SDK from [here](https://ru.coronalabs.com/product/)

- For scripting use [Sublime Text](https://www.sublimetext.com/3) or [Visual Studio Code](https://code.visualstudio.com/).

## Work with project

- Clone entire [project](https://github.com/MisterProper9000/PeopleClickerInception) to a local repository

```git
# clone project
git clone https://github.com/MisterProper9000/PeopleClickerInception
cd PeopleClickerInception
```

- Clone only [develop](https://github.com/MisterProper9000/PeopleClickerInception/tree/develop) branch to a local repository.
```git
# grab develop branch
git clone -b develop --single-branch https://github.com/MisterProper9000/PeopleClickerInception
cd PeopleClickerInception
```

- Create new branch (feature or hotfix) from develop branch.

- Start Corona Simulator and open project from cloned directory. In `Open project` dialog window choose `main.lua` file.

- Create, add, change files in your local repository, then commit changes to your branch to save them.

## Test application

- Application testing can be done through emulating Android device inside Corona Simulator or through building appliction and installing it to outer Android devices.

- In order to test different resolutions, use `View as` menu option in Corona Simulator. It supports many default resolutions in borderless mode plus user-created virtual devices (`Custom device...`).

## Build .apk file

- Application files can be built through `File->Build` option. Developer can define application name, version code and name, target app store and key alias.

- Corona SDK supports only `ARM-v7a` ABI package. Make sure that your test device and target device support such ABI set.


