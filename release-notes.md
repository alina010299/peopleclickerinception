# Release notes

## Development


## v0.2.0 - March 27th, 2019

`compare release/master`
[Commits](https://github.com/MisterProper9000/PeopleClickerInception/compare/master...develop)

New Features

+ Project Logo and icons

+ First cutscene

+ New units animations and actions

+ Update battle field buttons style

+ Simple login system with 3 profiles

+ Update battle field scroll system for better perfomance

+ Release build on Android ([People Clicker: Inception.apk](https://www.dropbox.com/sh/x6ue4fd3sl47c60/AACRJDeKWlh_i-ifG4Iw3s4Ma?dl=0))

Release aims

* Code refactoring

* Login system implementation

* Upgrade configs to handle different screen resolutions

* Unit's state machine implementation


## v0.1.0 - March 13th, 2019

Commits 

`compare release/master`

* [main_menu](https://github.com/MisterProper9000/PeopleClickerInception/commit/890ee391ad1baad3759deff7a4b1021dbb0c8972)
* [spawn_buttons](https://github.com/MisterProper9000/PeopleClickerInception/commit/34ee230bed7d84dacb2fad134e7a9587e36cf191)
* [battle_field](https://github.com/MisterProper9000/PeopleClickerInception/commit/5c1db17798f8542f417eca6754ffd6bf002ba9c8)
* [unit_class](https://github.com/MisterProper9000/PeopleClickerInception/commit/1832b1dc9d8f286bb42253e36278d55ee0954b5c)
* [stats_scale](https://github.com/MisterProper9000/PeopleClickerInception/commit/f4b31395a6d01432f771f87d346179ebc1aa331b)
* [release-1](https://github.com/MisterProper9000/PeopleClickerInception/commit/a8e9202ea3de9f1f196b13c19efa3fa317d1b5d0)


New Features

+ Main menu with key UI function `New game`

+ Simple battle field

+ Unit classes

+ All basic graphics (menu items, unit models, battle field, etc.)

+ Game features (battle field swap by touch; moral scale - not integrated yet)

+ Release build on Android ([People Clicker: Inception.apk](https://download.ru/files/SABEsVRj)
)

Release aims

* First look at People Clicker: Inception;

* First experience in team developing and building product from its parts

* Realizing problems in development planning