# People Clicker. Inception

### Product Description

[Product and functional description](https://docs.google.com/document/d/1L4HTOzLFJEYnj7LQDR1EQ2x6vJL3P7EcT5bzpbzPVqs/edit?usp=sharing)

### Launching instruction for users

Download APK from [here](https://www.dropbox.com/sh/x6ue4fd3sl47c60/AACRJDeKWlh_i-ifG4Iw3s4Ma?dl=0)

### Development team

1. [Plaksin Daniil](https://github.com/MisterProper9000)

2. [Sergeev Georgii](https://github.com/sergeevgk)

3. [Gritsaenko Nikita](https://github.com/Nikitagritsaenko)

4. [Zhukov Alexander](https://github.com/akzhukov)

5. [Alina Mitrofanova](https://github.com/Mitrofanova-Alina)

6. [Chepulis Mihail](https://github.com/MChepulis)

7. [Tchekmarev Mihail](https://github.com/MihailTchekmarev)

### Сommit requirements
1. Commit's title must be limited to 50 characters

2. Capitalize commit's title 

3. Do not end title of the commit with a period 

4. Use the imperative mood and start a description with verbs\
—— YES: Fix, Change, Release, Refactore, Add, Update ... \
—— NO: Fixed, Changing, More fixes ... 

5. Use the description to explain WHAT and WHY you do, not HOW

6. Mention commit task

### Git workflow

![Git workflow](https://hsto.org/storage/4bf7e68c/49e29c35/3a01bd6b/782a1be3.png)

#### master 

* Master branch contains releases - working version of product.

#### develop

* Develop branch is created from `master`. Contains all major changes of product (features).

#### feature

* Feature branches are created from `develop`. Contains exactly one new feature (new functionallity)

* Feature branch must be named `feature-number_issue-name_issue`. (feature-#12345-spawn_button)

#### hotfix

* Hotfix branches are created from master and develop in order to fix some bugs, which were found some steps after incorrect version.

* Feature branch must be named `hotfix-#number_issue-fix-number_fix`. (hotfix-#12346-fix-1)

#### release

* Release branches are created from develop in order to examine ready-to-release version of product. After reviewing branch is merged with `master` and `develop`.

* Release branch must be named `release-number_version`.

### Code style
The list of recommendations in this document was based on resources:
* https://github.com/luarocks/lua-style-guide
* http://lua-users.org/wiki/LuaStyleGuide

#### Indentation and formatting

* PeopleClickerInception is indented with 2 spaces. Example:

```lua
function scene:show(event)
  local sceneGroup = self.view
  local phase = event.phase
  if(phase == "will") then
    toGame = widget.newButton(
      {       
         width = 220,
         height = 50
      }
```
* One should not use tabs for indentation, or mix it with spaces.

#### Documentation and comment style

* Use a space after `--`. 

```lua
--bad
-- good
```

* Use simple [LuaDoc](http://keplerproject.github.io/luadoc/manual.html#howto) commenting style with tags `@author`, `@param`, `@return`. The following code defines a function and its documentation.

```lua
--- @author <github_name>
--- Define special sequences of characters.
-- For each pair (find, subs), the function will create a field named with
-- find which has the value of subs.
-- It also creates an index for the table, according to the order of insertion.
-- @param subs The replacement pattern.
-- @param find The pattern to find.
function def_escapes (find, subs)
   local special = { t = "\t", n = "\n", ['"'] = '"', ['\\'] = '\\', }
   find = gsub (find, "\\(.)", function (x) return %special[x] or x end)
   subs = gsub (subs, "\\(.)", function (x) return %special[x] or x end)
   escape_sequences.n = escape_sequences.n+1
   escape_sequences[escape_sequences.n] = find
   escape_sequences[find] = subs
end
```

* Use `TODO` and `FIXME` tags in comments. `TODO` indicates a missing feature
to be implemented later. `FIXME` indicates a problem in the existing code
(inefficient implementation, bug, unnecessary code, etc).

* Comment each `end` terminator, using the following example:

```lua
for i,v in ipairs(t) do
  if type(v) == "string" then
    ...lots of code here...
  end -- if string
end -- for each t
```
> **Clarification:** `end` is a terminator for many constructs in Lua, comment is used to clarify which exact construction is being terminated.

* In-method and module comments.
Comment lines, which contain abstruse (difficult to understand) code. Place your comment a line before. In modules clarify meaning of tables and metatables, if neccecary.

```lua
-- Module for class 'Sum', if possible named 'sum.lua'.

--- Sum is a class to sum two values.

-- Table, which holds two numbers for summarize 
local M = {}

--- Set the first value.
-- @param number value_a The first value.
M.set_a = function(self, value_a)
  self.value_a = value_a
end

--- Set the second value.
-- @param number value_b The second value.
M.set_b = function(self, value_b)
  self.value_b = value_b
end

--- Get sum of first and second value.
-- Can raise an error if the values cannot be summed.
-- @return number The sum of the first and second values.
M.get = function(self)
  return self.value_a + self.value_b
end
```

#### Variable names
* Variable names with larger scope should be more descriptive than those with smaller scope. One-letter variable names should be avoided except for iterators.

* `i`, `j`, `k`, etc. should be used only as a counter variable in for loops.

* When doing OOP and sceneGroup, classes should use `UpperCamelCase`. 

* Class methods and fields, and tables use `lowerCamelCase`.

* Variables and function names should use `snake_case`.

* Prefer using `is` when naming boolean functions:

- [ ] discuss Hungarian notation

> **Clarification:** Lua is a dynamically typed language, so it is convenient to know exact type of variable.

```lua
-- bad
local function evil(alignment)
  return alignment < 100
end

-- good
local function is_evil(alignment)
  return alignment < 100
end
```
#### Variable declaration
* Always use `local` to declare variables. 

```lua
-- bad
scene = composer.newScene()

-- good
local scene = composer.newScene()
```

#### Tables

- [ ] discuss this

* When creating a table, prefer populating its fields all at once, if possible:

```lua
local unit = {
  hp = 1,
  class = "Soldier"
}
```
#### Strings

* Use `"double quotes"` for strings; use `'single quotes'` when writing strings
that contain double quotes.

#### Function declaration syntax

* Prefer function syntax over variable syntax. This helps differentiate between
named and anonymous functions.

```lua
-- bad
local foo = function(name, options)
  -- ...stuff...
end

-- good
local function foo(name, options)
  -- ...stuff...
end
```
#### Table attributes

* Use dot notation when accessing known properties.

```lua
local luke = {
  jedi = true,
  age = 28,
}

-- bad
local is_jedi = luke["jedi"]

-- good
local is_jedi = luke.jedi
```

* Use subscript notation `[]` when accessing properties with a variable or if using a table as a list.

```lua
local vehicles = load_vehicles_from_disk("vehicles.dat")

if vehicles["Porsche"] then
  porsche_handler(vehicles["Porsche"])
  vehicles["Porsche"] = nil
end
for name, cars in pairs(vehicles) do
  regular_handler(cars)
end
```

> **Clarification:** Using dot notation makes it clearer that the given key is meant to be used as a record/object field.

#### Functions in tables

* When declaring modules and classes, declare functions external to the table definition:

```lua
local my_module = {}

function my_module.a_function(x)
  -- code
end
```

#### Conditional expressions
- [ ] discuss this

#### Blocks

* Use single-line blocks only for `then return`, `then break` and `function return` (a.k.a "lambda") constructs:

```lua
-- good
if test then break end

-- good
if not ok then return nil, "this failed for this reason: " .. reason end

-- good
use_callback(x, function(k) return k.last end)

-- good
if test then
  return false
end

-- bad
if test < 1 and do_complicated_function(test) == false or seven == 8 and nine == 10 then do_other_complicated_function() end

-- good
if test < 1 and do_complicated_function(test) == false or seven == 8 and nine == 10 then
  do_other_complicated_function() 
  return false 
end
```

* Separate statements onto multiple lines. Do not use semicolons as statement terminators.

```lua
-- bad
local whatever = "sure";
a = 1; b = 2

-- good
local whatever = "sure"
a = 1
b = 2
```

#### Spacing

* Always put a space after commas and between operators and assignment signs:

```lua
-- bad
local x = y*9
local numbers={1,2,3}
numbers={1 , 2 , 3}
numbers={1 ,2 ,3}
dog.set( "attr",{
  age="1 year",
  breed="Bernese Mountain Dog"
})

-- good
local x = y * 9
local numbers = {1, 2, 3}
dog.set("attr", {
  age = "1 year",
  breed = "Bernese Mountain Dog",
})
```

* Indent tables and functions according to the start of the line, not the construct:

```lua
-- bad
local my_table = {
                   "hello",
                   "world",
                 }
using_a_callback(x, function(...)
                       print("hello")
                    end)

-- good
local my_table = {
  "hello",
  "world",
}
using_a_callback(x, function(...)
  print("hello")
end)
```

* No spaces after the name of a function in a declaration or in its arguments:

```lua
-- bad
local function hello ( name, language )
  -- code
end

-- good
local function hello(name, language)
  -- code
end
```

* Add blank lines between functions:

```lua
-- bad
local function foo()
  -- code
end
loсal function bar()
  -- code
end

-- good
local function foo()
  -- code
end

local function bar()
  -- code
end

#### Errors

* Functions that can fail for reasons that are expected (e.g. I/O) should
return `nil` and a (string) error message on error, possibly followed by other
return values such as an error code.
```

#### Modules

* Always require a module into a local variable named after the last component of the module’s full name.

```lua

-- requiring the module
local bar = require("foo.bar") 

-- using the module
bar.say("hello") 
```

* Try to use names that won't clash with your local variables. For instance, don't name your module something like “size”.


###