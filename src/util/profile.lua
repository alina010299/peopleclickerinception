-- @author MСhepulis
-- @author MisterProper9000
-- хранит, модифицирует, сохраняет/загружает профили пользователей
-- 

local M = {}

local loadsave = require("util.loadsave")


-- название файла, в котором хранятся данные о пользователях
local profilesFileName = "users.json"

local H = display.contentHeight
local W = display.contentWidth

-- структура для хранения данных о провилях 
local profiles = {
  active = "",                -- текущий активный пользователь

  file1 = {
    id = 1,                   -- уникальный номер пользователя
    cur_lvl = 0               -- текущий уровень
  },

  file2 = {
    id = 2,
    cur_lvl = 0    
  },

  file3 =  {
    id = 3,
    cur_lvl = 0    
  }
}


-- @author MСhepulis
-- возвращает активного юзера, если такого нет, то nil
-- @return активный пользователь
function M.getActive() 
  if(profiles.active == "") then return nil end
  return profiles.active
end

-- @author MСhepulis
-- очищаем данные юзера
-- создаёт диалоговое окно для подтверждения выбора пользователя
-- после получения ответа удаляет диалоговое окно
function deleteUser(num) -- очищаем данные юзера
  -- для удобста адресции в массиве имена кнопок подобраны согласно именам полей
  -- чтобы не использовать string.sub
 

  -- print(event.target.name)  
  -- profiles["fil"..event.name].cur_lvl = 0  --такая адресация не работает т.к. event.name - имя ивента ("tap"), а не того от кого пришло
  -- print(num)
  

  local back = display.newRect(W/2, H/2, W, H) 
  back.alpha = 0.2
  back:addEventListener("touch", function() return true end)
  back:addEventListener("tap", function() return true end)

  local message = display.newRect(W/2, H/2, W/2, H/2) 
  message:setFillColor(0.8, 0.8, 0.8)

  local message_text = display.newText("Are you shure?", message.x, message.y, native.systemFont)
  message_text.fontSize = H/50
  message_text.alig = "center"
  message_text:setFillColor(0, 0, 0 )

  local yes_button = display.newRect(message.x - message.width/4, message.y + message.height/4, message.width/4, message.height/6) 
  yes_button:setFillColor(0, 1, 0)

  local no_button  = display.newRect(message.x + message.width/4, message.y + message.height/4, message.width/4, message.height/6) 
  no_button:setFillColor(1, 0, 0)

  local yes_text = display.newText("Yes", yes_button.x, yes_button.y, native.systemFont)
  yes_text.fontSize = H/50
  yes_text.alig = "center"
  yes_text:setFillColor(0, 0, 0 )

  local no_text = display.newText("No", no_button.x, no_button.y, native.systemFont)
  no_text.fontSize = H/50
  no_text.alig = "center"
  no_text:setFillColor(0, 0, 0 )


  local function closeDialog()
    yes_button:removeEventListener("tap", yes_button)
    no_button:removeEventListener("tap", no_button)
    back:removeSelf()
    message:removeSelf()
    message_text:removeSelf()
    yes_button:removeSelf()
    no_button:removeSelf()
    yes_text:removeSelf()
    no_text:removeSelf()
  end

  function yes_button:tap(event)
    profiles["file"..num].cur_lvl = 0  
    closeDialog()   
  end  
  
  function no_button:tap(event)
    closeDialog()  
  end

  yes_button:addEventListener("tap", yes_button)
  no_button:addEventListener("tap", no_button)

end

--- @autor MisterProper9000 теперь эта функция не нужна и её функции исполняет setActive 
-- @author MСhepulis
-- изменяем активного пользователя
--function logIn(num) -- устанавливаем поле active 
  -- для удобста адресции в массиве имена кнопок подобраны согласно именам полей
  -- print("ggggg")
  -- print(num)
  --profiles.active = profiles[event.name]  --такая адресация не работает т.к. event.name - имя ивента ("tap"), а не того от кого пришло
  --profiles.active = profiles["file"..num]
  --M.save()
  --print("Active profile", profiles.active.id)
  --print(profiles.active.id)
--end


-- @author MСhepulis
-- открывает файл и заполняет таблицу, если файла нет, то nil
-- @return таблица считаная из файла, если не смогли открыть файл, то nil
function M.init() 
  local table = loadsave.loadTable(profilesFileName, system.DocumentDirectory)
  if(not table) then return nil end
  for key, value in pairs(table) do
  	profiles[key] = value
  end
  return true
end


-- @author MСhepulis
-- сохраняем таблицу в файл
function M.save() 
  loadsave.saveTable(profiles, profilesFileName, system.DocumentDirectory)
end



----------------- работа с окном ввода

-- FIXME добавить картинку всем прямоугольникам
local windows_options = {
  y = H * 5 / 11,
  width =  H * 3 / 5 * 248/348,
  height = H * 3 / 5,
  img = ""
}

local del_windows_options = {
  y = H * 3 / 4 + windows_options.height * 1 / 5,
  width = windows_options.width,
  height = windows_options.width * 62/248,
  img = ""
}

-- local wnd_1_x = W * 3/ 12
-- local wnd_2_x = W * 1 / 2
-- local wnd_3_x = W * 9 / 12

local wnd_1_x = W * 1/6
local wnd_2_x = W * 1/2
local wnd_3_x = W * 5/6

local background
local file1
local file2
local file3
local delete1
local delete2
local delete3


-- @author MСhepulis
-- создаёт картинки трёх профилей, позволяя пользователю выбрать один из профилей или удалить текущий прогресс в профиле
function M.activateLogInDialog()
  -- ERROR review akzhukov
  -- нет local у переменных(но мб так и надо? вряд ли)
  -- стоит их объявить где-то повыше 
  -- FIX MisterProper9000: 
  -- добавлены объявления выше с local
  background = display.newRect(W/2, H/2, W, H) 
  background:setFillColor(194/255, 179/255, 198/255)
  file1 = display.newImageRect("util/profile_resources/progress1.png", windows_options.width, windows_options.height)
  file1.x = wnd_1_x
  file1.y = windows_options.y

  file2 = display.newImageRect("util/profile_resources/progress2.png", windows_options.width, windows_options.height)
  file2.x = wnd_2_x
  file2.y = windows_options.y

  file3 = display.newImageRect("util/profile_resources/progress3.png", windows_options.width, windows_options.height)
  file3.x = wnd_3_x
  file3.y = windows_options.y

  -- ERROR review akzhukov
  -- "e1" очень плохое название
  -- FIX MisterProper9000
  -- change to delete1,2,3
  delete1 = display.newImageRect("util/profile_resources/delete.png", del_windows_options.width, del_windows_options.height)
  delete1.x = wnd_1_x
  delete1.y = del_windows_options.y

  delete2 = display.newImageRect("util/profile_resources/delete.png", del_windows_options.width, del_windows_options.height)
  delete2.x = wnd_2_x
  delete2.y = del_windows_options.y

  delete3 = display.newImageRect("util/profile_resources/delete.png", del_windows_options.width, del_windows_options.height)
  delete3.x = wnd_3_x
  delete3.y = del_windows_options.y

  function SetActive(event)
    if (event.target == file1) then
      profiles.active = profiles["file1"]
    elseif (event.target == file2) then
      profiles.active = profiles["file2"]
    elseif (event.target == file3) then
      profiles.active = profiles["file3"]
    end
    print("Active profile", profiles.active.id)
    M.save()
    M.closeLogInDialog()
  end
   
  -- FIX MisterProper9000
  -- MСhepulis пытался замаскировать вызов функций SetActive под функтор. Так не надо. При этом решено оставить функторы ретурн тру, потому что мелочь
  background:addEventListener("touch", function() return true end)
  background:addEventListener("tap", function() return true end)
  file1:addEventListener("tap", SetActive)
  file2:addEventListener("tap", SetActive)
  file3:addEventListener("tap", SetActive)
  delete1:addEventListener("tap", deleteUser)
  delete2:addEventListener("tap", deleteUser)
  delete3:addEventListener("tap", deleteUser)

end

-- @author MСhepulis
-- закрывает окно выбора профиля
function M.closeLogInDialog()

  -- ERROR review akzhukov
  -- почему это закомменчено? listener dont remove
  -- FIX MisterProper9000
  -- uncomment
  file1:removeEventListener("tap", SetActive)
  file2:removeEventListener("tap", SetActive)
  file3:removeEventListener("tap", SetActive)
  delete1:removeEventListener("tap", deleteUser)
  delete2:removeEventListener("tap", deleteUser)
  delete3:removeEventListener("tap", deleteUser)


-- если использовать background:removeSelf(), то мы уже не имеем право вызывать closeLogInDialog() без предварительного вызова activateLogInDialog(), что небезопасно
  --background:removeSelf()
  --file2:removeSelf()
  --file3:removeSelf()
  --delete1:removeSelf()
  --delete2:removeSelf()
  --delete3:removeSelf()
  
-- поэтому используется display.remove
  display.remove(background)
  display.remove(file1)
  display.remove(file2)
  display.remove(file3)
  display.remove(delete1)
  display.remove(delete2)
  display.remove(delete3)

 
end

return M