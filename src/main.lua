	-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local composer = require("composer")

-- Removes status bar on iOS
display.setStatusBar( display.HiddenStatusBar ) 

-- Removes bottom bar on Android 
if system.getInfo( "androidApiLevel" ) and system.getInfo( "androidApiLevel" ) < 19 then
  native.setProperty( "androidSystemUiVisibility", "lowProfile" )
else
  native.setProperty( "androidSystemUiVisibility", "immersiveSticky" ) 
end

-- ADD: check if this is the first launch (or we could show logo at each 10th launch, for example, i don't know...) 
-- Or check the date of last launch and if ther is more than 24 hours then show logo, otherwise go directly to the main menu
composer.gotoScene("cutscenes.Logo", {700, effect = "fade"})