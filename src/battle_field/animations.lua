-----------------------------------------------------------------------------------------
--
-- animations.lua
--
-----------------------------------------------------------------------------------------
--- @author <Nikitagritsaenko>
-- Здесь предоставлен интерфейс для создания анимаций извне (это нужно только при работе с юнитами
-- то есть в файле unit.lua) 

-- ---------------------СОЗДАНИЕ И ХРАНЕНИЕ АНИМАЦИЙ---------------------------------------------------------
-- статическая таблица с данными для конструирования анимации
AnimationDataList = {}

function AnimationDataList:new()
  local public = {}
  public.animList = {}

  -- Создание аргументов конструктора, по которым будет собрана анимация. Эти аргументы создаются только один
  -- раз и кладутся в статический список AnimationDataList
  -- @param animation_name имя файлов: листа со спрайтами, а также файла .lua, где указано, как работать с этим листом
  function create_anim_data(animation_name)
    local sheet = require("battle_field.animations."..animation_name) 
    local concrete_unit_sheet = graphics.newImageSheet("battle_field/animations/"..animation_name..".png", sheet:getSheet()) 
    local sheet_data = { 
      {
        name = "Run",
        frames = { 8,9,10,11,12,13,14 },
        time = 550, 
        count = 7
      },
      {
        name = "Death1",
        frames = { 1,2,3,4,5,6,7 },
        time = 800, 
        loopCount = 1,
        count = 7
      }
    }
    public.animList[animation_name] = {concrete_unit_sheet, sheet_data}
  end
  
  function AnimationDataList:getAnimationData(animation_name)
    return public.animList[animation_name]
  end

  -- сюда добавляем новые анимации, созданные Даниилом
  create_anim_data("soldier")

  setmetatable(public, self)
  self.__index = self; return public
end

-- Проверка таблицы на пустоту
-- @param t таблица, которую проверяем на пустоту
function is_empty(t)
  for _,_ in pairs(t) do
    return false
  end -- for each t
  return true
end

-- эта таблица выполняет роль синглтона для хранения данных анимаций
-- она создается только при первом запросе функции get_animation
anim_data_list = {}

--- @author <Nikitagritsaenko>
--- Создание анимации 
-- @param animation_name имя анимации (например, "SoldierRun")
-- @param
-- @return объект анимации (display.newSprite)
function get_animation(animation_name)
  -- синглтон данных для конструирования анимаций
  if is_empty(anim_data_list) then
    anim_data_list = AnimationDataList:new()
  end
   
  -- по имени анимации достаём данные о ней
  local anim_data = AnimationDataList:getAnimationData(animation_name)
  local sheet = {}
  local sheet_data = {}
  
  -- конструируем части анимацию по данным, извлеченным выше
  for key, value in pairs(anim_data) do
    if (key % 2 ~= 0) then
      sheet = value
    else
      sheet_data = value
    end
  end -- for each key, value
  -- создаём объект анимации по данным, собранным выше
  local animation = display.newSprite(sheet, sheet_data)
  animation:scale(1.2, 1.2)
  return animation
end