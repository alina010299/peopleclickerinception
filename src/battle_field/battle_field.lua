local composer = require( "composer" )
local units = require ("battle_field.unit")
local widget = require( "widget" )
local loadsave = require("util.loadsave")

local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


 
 local W = display.contentWidth
 local H = display.contentHeight
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
local function checkMemory()
    collectgarbage( "collect" )
    local memUsage_str = string.format( "MEMORY = %.3f KB", collectgarbage( "count" ) )
    print( memUsage_str, "TEXTURE = "..(system.getInfo("textureMemoryUsed") / (1024 * 1024) ) )
end

--- @autor MisterProper9000
--- объявление таймера
local cooldown_timer

--- @autor Mitrofanova-Alina
--- Handle all events of manipulating the scroll view
-- @param event
local function scrollListener( event )
    local phase = event.phase
    local direction = event.direction

    --- later TODO: function can react to begin move and end phase of the event

    return true
end


  -- ERROR review akzhukov
  -- стоит поправить стиль кодирования во всем файле, а то написано где-то lowCamel где-то snake_case
  -- надо почти везде написать lowCamel
  -- FIX MisterProper9000
  -- gjabrcbk почти везде?
--------------------------------
local scrollView 
-- ERROR review akzhukov
-- newGroup не надо здесь писать, а  в create или show
-- Fix MisterProper9000
-- Да, писать надо в scene:show. И сделать ремув в scene:hide
local unitGroup --= display.newGroup() -- группа для юнитов, нужна как отдельная группа для сортировки по "y"
local spawnButtonsGroup

local current_unit -- type юнита, которого нужно заспавнить
local number_line -- линия, на которую нужно заспавнить юнита

local buttons_lines = {
    {buttonName = button_line_1, fileName = "battle_field/line.png", centerY},
    {buttonName = button_line_2, fileName = "battle_field/line.png", centerY},
    {buttonName = button_line_3, fileName = "battle_field/line.png", centerY}
}

local number_units = 8 -- должно быть четным

-- review MisterProper
-- Потенциально проблемное место
local unit_size = 0.8*(H/number_units*2 - H/50) -- размеры иконки юнита

local battleFieldImage

local shift = H/75 -- расстояние между кнопоками о отступ от края экрана
local buttons_units = {

    soldier = 
    {type = "soldier", buttonName = button_spawn_unit_1, fileName = "battle_field/soldier_icon.png",
        top = (unit_size+shift)*0+shift, left = shift , cooldown = 1},

    machineGunner = 
    {type = "machineGunner", buttonName = button_spawn_unit_2, fileName = "battle_field/gunner_icon.png",   
        top = (unit_size+shift)*0+shift, left = shift + unit_size + shift, cooldown = 5},

    sniper = 
    {type = "sniper", buttonName = button_spawn_unit_3, fileName = "battle_field/sniper_icon.png",  
        top = (unit_size+shift)*1+shift, left = shift, cooldown = 2},

    bazookaMan = 
    {type = "bazookaMan", buttonName = button_spawn_unit_4, fileName = "battle_field/grenadier_icon.png",  
        top = (unit_size+shift)*2+shift, left = shift, cooldown = 3},

    officer = 
    {type = "officer", buttonName = button_spawn_unit_5, fileName = "battle_field/officer_icon.png", 
        top = (unit_size+shift)*1+shift, left = shift + unit_size + shift, cooldown = 6},

    heavyMachineGunner = 
    {type = "heavyMachineGunner", buttonName = button_spawn_unit_6, fileName = "battle_field/heavy_gunner_icon.png",
        top = (unit_size+shift)*2+shift, left = shift + unit_size + shift, cooldown = 7},

    mortar = 
    {type = "mortar", buttonName = button_spawn_unit_7, fileName = "battle_field/mortar_icon.png", 
        top = (unit_size+shift)*3+shift, left = shift, cooldown = 4},

    tank = 
    {type = "tank", buttonName = button_spawn_unit_8, fileName = "battle_field/tank_icon.png",
        top = (unit_size+shift)*3+shift, left = shift + unit_size + shift, cooldown = 8},
}

-- анимация отката юнита
-- чтобы сделать в это время кнопку неактивной ставится прозрачность на максимум
-- затем рисуется картинка повторяющая иконку кнопки, а на ней маска и анимацией
local function radialEffect()
    -- важно локально запомнить curr, чтобы по окончанию таймера вернуть кнопку нужному юниту,
    -- иначе при спавне нескольких юнитов сразу откатится корректно откатится только последний
    local curr = current_unit
    buttons_units[curr].buttonName:setFillColor(1, 1, 1, 0)
    
    -- ERROR review MisterProper9000
    -- Почему не пояснено что такое image_background? Надо написать что это такое.
    local image_background = display.newImageRect( buttons_units[curr].fileName, unit_size, unit_size )
    image_background.x = buttons_units[curr].left + unit_size/2
    image_background.y = buttons_units[curr].top + unit_size/2 - 1
    spawnButtonsGroup:insert(image_background)

    local mask = display.newImageRect( "battle_field/spawnButtonMask.png", unit_size, unit_size )
    mask.x = buttons_units[curr].left + unit_size/2
    mask.y = buttons_units[curr].top + unit_size/2 - 1
    mask:setFillColor(1, 1, 1, 0.75) -- 4 параметр - прозрачность маски, первые три параметра это цвет, для картинки смысла не имеют
    spawnButtonsGroup:insert(mask)

    -- написано 20 подобрано для плавности анимации откат
    --(в произведении со строкой вызова таймера 20*50=1000, т.е. 1 сек)
    local cd = buttons_units[curr].cooldown * 20 
    
    local time_limit = cd

    local function timerDown()
        time_limit = time_limit - 1

        mask.fill.effect = "filter.radialWipe"
        mask.fill.effect.center = { 0.5, 0.5 }
        mask.fill.effect.smoothness = 0
        mask.fill.effect.axisOrientation = 0.75
        mask.fill.effect.progress = 1 - time_limit / cd
        if (time_limit == 0) then
            buttons_units[curr].buttonName:setFillColor(1, 1, 1, 1)
            mask:removeSelf()
            image_background:removeSelf()
        end
    end

    -- ERROR review akzhukov
    -- local нету у таймера
    -- QUESTION review MisterProper9000
    -- Удаляется ли автоматически таймер с заданным числом итераций??
    -- FIX MisterProper9000 
    -- Добавил объявление с local выше и добавил пока закоменченное удаление таймера в scene:hide
    cooldown_timer = timer.performWithDelay(50, timerDown, time_limit)  
end


-- вызывается при нажатии на юнита и предлагает выбрать линии
local function selectLine(event)
    if (current_unit == event.target.id) then
        for i = 1, #buttons_lines do
            buttons_lines[i].buttonName:setFillColor(1, 1, 1, 0)
        end
        current_unit = nil
    else
        for i = 1, #buttons_lines do
            buttons_lines[i].buttonName:setFillColor(1, 1, 1, 0.75)
        end
        current_unit = event.target.id --в id записан type
    end
    
end

-- вызывается после выбора линии и запускает откат юнита
local function unitSpawned(event)
    for i = 1, #buttons_lines do
        buttons_lines[i].buttonName:setFillColor(1, 1, 1, 0)
    end
    number_line = event.target.id 
    radialEffect()

    print("Unit with id "..current_unit.." on line "..number_line )
    
    -- передача в функцию спавна данных о юните(или отряде?) и линии

    local tmp = Soldier:new(unitGroup, number_line, 1, current_unit) -- создаем юнит
    tmp:setState("Run") -- это точно тут надо писать? в конструкторе никак? 

    -----------------

    current_unit = nil --важная строка, если тут не занулить юнита, то нужно будет дабл кликать чтобы заспавнить
    
end


-- create()
function scene:create( event )
    local sceneGroup = self.view
 
end


 
-- show()
function scene:show( event )

    local sceneGroup = self.view

    local phase = event.phase
    local rectWidth = shift * 3 + unit_size * 2
    local set_rightPadd -- necessary for proper scrolling with spawn buttons mode (left or right)

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        spawnButtonsGroup = display.newGroup()

        local settingsData = loadsave.loadTable("data/settings.json", system.ResourceDirectory) -- settings file
        local spawnButtonsMode = settingsData.spawnButtonsMode -- button positions - left or right
        if spawnButtonsMode == "left" then
            set_rightPadd = 0
        else
            set_rightPadd = rectWidth
        end

    --- @autor Mitrofanova-Alina
    --- Determines scroll by touch screen settings
        scrollView = widget.newScrollView
        {
            left = 0,
            top = 0,
            width = W,
            height = H,
            topPadding = 0,
            bottomPadding = 0,
            rightPadding = set_rightPadd,
            leftPadding = 0,
            isBounceEnabled = false,
            horizontalScrollDisabled = false,
            verticalScrollDisabled = true,
            listener = scrollListener,
        }
        local lvlData = loadsave.loadTable("data/lvl1.json", system.ResourceDirectory)
        battleFieldImage = display.newImageRect(lvlData.map.image, lvlData.map.W, lvlData.map.H)

        -- alignment of the object along the x direction
        -- 0 is to left alignment
        battleFieldImage.anchorX = 0

        -- specifie the x and y position
        if spawnButtonsMode == "left" then
            battleFieldImage.x = rectWidth -- need to move the map from 0 to size of spawn buttons
        else
            battleFieldImage.x = 0
        end
        battleFieldImage.y = H/2;

        scrollView:setScrollWidth(lvlData.map.W + battleFieldImage.x) -- set the scroll width, depending on the position of the buttons
        scrollView:insert(battleFieldImage)

        unitGroup = display.newGroup() -- @autor MisterProper9000

        scrollView:insert(unitGroup)
        sceneGroup:insert(scrollView)

        -- создание кнопок юнитов
        local backgroundButtons = display.newRect(0, 0, rectWidth, H)
        backgroundButtons:addEventListener("touch", function() return true end)
        backgroundButtons:addEventListener("tap", function() return true end) 
        backgroundButtons.anchorX = 0
        backgroundButtons.x = 0;
        backgroundButtons.y = H/2;
        spawnButtonsGroup:insert(backgroundButtons)
        backgroundButtons:setFillColor(0, 0, 0, 1)

        local fileButton, enable

        for key, val in pairs(lvlData.player) do
            if (val) then
                fileButton = buttons_units[key].fileName
                enable = true
            else
                fileButton = "battle_field/lock.png"
                enable = false
            end
                buttons_units[key].buttonName = widget.newButton(
                    {

                        id = buttons_units[key].type,
                        width = unit_size,
                        height = unit_size,
                        top = buttons_units[key].top,
                        left = buttons_units[key].left,
                        defaultFile = fileButton ,
                        isEnabled = enable,
                        onPress = selectLine
                    }
                )
                spawnButtonsGroup:insert(buttons_units[key].buttonName)
        end
        if spawnButtonsMode == "left" then -- x position of buttons depends on these mode (left or right)
            spawnButtonsGroup.x = 0
        else
            spawnButtonsGroup.x = W - rectWidth
        end
    -- создание кнопок линий  
     for i = 1, #buttons_lines  do
        buttons_lines[i].buttonName = widget.newButton(
            {
                
                id = i,
                width = W,
                height = H/4,
                top = (i) * (H/4),
                left = 0, 
                defaultFile = buttons_lines[i].fileName,
                isEnabled = true,
                onPress = unitSpawned
            }
        )
        buttons_lines[i].centerY =  i * H/4 + H/8,
        -- изначаль кнопки на линии полность прозрачными
        buttons_lines[i].buttonName:setFillColor(1, 1, 1, 0)
        sceneGroup:insert(buttons_lines[i].buttonName)
    end
    sceneGroup:insert(spawnButtonsGroup)  

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        --timer.cancel(cooldown_timer) -- останавливаем таймер


    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
        for i = 1, #buttons_lines do
            buttons_lines[i].buttonName:removeSelf()
        end
        
        for i = 1, #buttons_units do
            buttons_units[i].buttonName:removeSelf()
        end
        
        unitGroup:removeSelf()

        scrollView:removeSelf()

        display.remove(backgroundButtons)
        backgroundButtons = nil

        display.remove(battleFieldImage)
        battleFieldImage = nil

        display.remove(battle_field_group)
        battle_field_group= nil
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene