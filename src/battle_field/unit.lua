-----------------------------------------------------------------------------------------
--
-- unit.lua
--
-----------------------------------------------------------------------------------------
--- @author <Nikitagritsaenko>
-- Создан класс юнита.
-- Для удобства храним список юнитов в отдельном классе (UnitList)
-- NB! Хранение списка переедет в модуль Level

local anim = require "battle_field.animations" 
local saveload = require "util.loadsave"
-- -----------------------------------------------------------------------------------------
--- сортировка группы по оси "y"
-- @param displayGroup группа
local function depthSort(displayGroup)
	local objects = {}
	for i=1,displayGroup.numChildren do objects[#objects+1] = {displayGroup[i].y, displayGroup[i]} end
	table.sort( objects, function(a,b) return a[1]<b[1] end )
	for i=1,#objects do displayGroup:insert( objects[i][2] ) end
end
-- ---------------------КЛАСС ЮНИТА---------------------------------------------------------
Unit = {}

--- @author <Nikitagritsaenko>
--  @param display_group группа, в которой находится юнит
--  @param line линия, на которой юнит находится
--  @param team команда (1 - игрок, иначе - АИ)
--  @param type тип юнита (солдат, снайпер, ...)
function Unit:new(display_group, line, team, type) 
    -- данные по type считываются из конфиг файла
    local public = saveload.loadTable("data/units.json", system.ResourceDirectory)[type]

    -- данные, которые берём не из конфига
    public.type = type
    public.state = "Run" -- дефолтное состояние
    public.line = line
    public.team = team -- TODO: пусть team будет передаваться в уровень (отряд хранит список и команду - как минимум)
    public.display_group = display_group
    public.isCovered = false -- юнит изначально появляется не в укрытии
    public.squad = nil
    public.animation = nil

    -- set-методы
    function public:setState(state) 
      public.state = state 
      public:setAnimation()
    end

    -- get-методы
    function public:getX() return public.x end

    function public:getLine() return public.line end

    function public:getHP() return public.HP end

    function public:getTeam() return public.team end
    
    function public:getMeleeDamage() return public.meleeDamage end

     -- Squad вызывает этот метод, когда заполняет таблицу с уроном для передачи вражескому отряду
    function public:getShootingDamage()
      if (public.shootingDelay == 0 and public.attackSpeed > 0) then
        return public.shootingDamage
      else
        return 0
      end
    end

    -- движение анимации по оси x, вызывается каждый кадр
    function public:move()
      if (public.animation ~= nil and public.state == "Run") then
        if (public.animation.x < 1500) then
          public.animation.x = public.animation.x + public.movementSpeed
        else
          public:setState("Death1")
          Runtime:removeEventListener("enterFrame", public)
        end
      end
    end

    -- каждый кадр вызываем то, что внутри этой функции
    function public:enterFrame(event) 
        public:move()
    end

    --- @author <Nikitagritsaenko>
    --- Функция вызывается при смене состояния юнита, переключает анимацию
    function public:setAnimation() 
      if (public.animation ~= nil) then -- меняем анимацию
        local tmp_x = public.animation.x
        local tmp_y = public.animation.y
        display.remove(public.animation)
        public.animation:setSequence(public.state)
        public.animation.x = tmp_x
        public.animation.y = tmp_y
      else -- если создаём в первый раз
        -- начальную позицию по x и y будем генерировать случайно в некотором диапазоне
        -- для координаты "y" этот диапазон зависит от номера линии, для "x" сделаем фиксированный диапазон 
        -- второе слагаемое в след. строке нужно, чтобы юнит сильно не вылезал над своей линией
        -- это особенно заметно, если юнит находится на самой верхней линии, может случиться так, что на фоне юнита будет
        -- практически лишь белый фон, а не фон линии
        -- NB! Для этого экран должен быть поделен на 4 РАВНЫЕ по высоте линии, чего я не вижу в текущей версии, так что на верхней
        -- линии юнит может заспавниться слишком высоко
        public.animation = get_animation(public.type) -- создание новой анимации
        public.animation:setSequence(public.state) -- выбираем состояние

        local up_y_frontier = public.line * 2 * display.contentHeight / 8 + public.animation.height / (3 * 4) 
        local down_y_frontier = (public.line + 1) * 2 * display.contentHeight / 8
        public.animation.x = math.random(500, 500 + display.contentWidth / 8)
        public.animation.y = math.random(up_y_frontier, down_y_frontier)
      end
      public.animation.anchorY = 1 -- привязываем точку отчета по оси "y" к ступням юнита
      public.display_group:insert(public.animation)
      depthSort(public.display_group)
      public.animation:play()
      Runtime:addEventListener("enterFrame", public)  
      --public.animation:addEventListener("tap", onTouch)
    end

    -- юнит получает урон
    function public:takeDamage(damage) public.HP = public.HP - damage end 
    
    setmetatable(public,self)
    self.__index = self; return public
end
---------------------------------------------------------------------------------------------------------------
-- Наследование - пока не пригодилось, но скорее всего пригодится для переопределения методов
-- @param child класс-наследник
-- @param parent базовый класс
function extended (child, parent)
  setmetatable(child,{__index = parent}) 
end

Soldier = {}
extended(Soldier, Unit)

-- ---------------------ХРАНЕНИЕ ЮНИТОВ---------------------------------------------------------
-- TODO: этот код (или его часть) передеет в модуль Level
-- таблица (класс), хранящая всех юнитов, а именно 2 списка: команды игрока и команды "AI"
UnitList = {}
function UnitList:new()
    local public = {}
    public.units1 = {}  
    public.units2 = {} 
    
    function public:addUnit(newUnit)
      if (newUnit:getTeam() == 1) then
        public.units1[newUnit] = true 
      else
        public.units2[newUnit] = true         
      end
    end

    -- функция, которая будет проверять, живы ли еще юниты; те, которых уничтожили в игре, будут удалены из списка
    function public:removeDead()
      for key, val in pairs(public.units1) do
        if (key:getHP() <= 0) then
          public.units1[key] = nil 
        end
      end -- for each key, val
      for key, val in pairs(public.units2) do
        if (key:getHP() <= 0) then
          public.units2[key] = nil 
        end
      end -- for each key, val
    end
  
  
  setmetatable(public, self)
  self.__index = self return public
end

-- TODO
-- когда тапаем по юниту, то уведомляем отряд о том, что он выбран, отряд говорит UI отрисовать кнопки приказов,
-- при этом, если там уже были кнопки другого отряда - он уведомляется о том, что перестал быть выбранным
function onTouch(event)
  -- event.target.x = event.target.x + 10
  return true
end 


      