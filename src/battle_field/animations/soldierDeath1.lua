local M = {}
M.sheetData = {
	frames = {
		{ name=Armature_death1_0, x = 301, y = 125, width = 56, height = 127, sourceX=147, sourceY=0, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_1, x = 301, y = 0, width = 94, height = 123, sourceX=104, sourceY=3, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_2, x = 174, y = 96, width = 125, height = 122, sourceX=69, sourceY=6, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_3, x = 0, y = 96, width = 172, height = 70, sourceX=15, sourceY=61, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_4, x = 0, y = 168, width = 171, height = 44, sourceX=10, sourceY=91, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_5, x = 0, y = 48, width = 177, height = 46, sourceX=4, sourceY=91, sourceWidth=203 , sourceHeight=138 },
		{ name=Armature_death1_6, x = 0, y = 0, width = 180, height = 46, sourceX=1, sourceY=91, sourceWidth=203 , sourceHeight=138 }
	},
	sheetContentWidth = 512,
	sheetContentHeight = 256
}
function M:getSheet()
    return self.sheetData;
end
return M