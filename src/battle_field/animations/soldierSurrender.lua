local M = {}
M.sheetData = {
	frames = {
		{ name=Armature_surrender_0, x = 61, y = 0, width = 48, height = 127, sourceX=0, sourceY=0, sourceWidth=60 , sourceHeight=127 },
		{ name=Armature_surrender_1, x = 159, y = 0, width = 46, height = 127, sourceX=0, sourceY=0, sourceWidth=60 , sourceHeight=127 },
		{ name=Armature_surrender_2, x = 207, y = 0, width = 44, height = 127, sourceX=0, sourceY=0, sourceWidth=60 , sourceHeight=127 },
		{ name=Armature_surrender_3, x = 0, y = 0, width = 59, height = 127, sourceX=0, sourceY=0, sourceWidth=60 , sourceHeight=127 },
		{ name=Armature_surrender_4, x = 111, y = 0, width = 46, height = 127, sourceX=0, sourceY=0, sourceWidth=60 , sourceHeight=127 }
	},
	sheetContentWidth = 256,
	sheetContentHeight = 256
}
function M:getSheet()
    return self.sheetData;
end
return M