local M = {}
M.sheetData = {
	frames = {
		{ name=1, x = 0, y = 123, width = 70, height = 121, sourceX=0, sourceY=3, sourceWidth=74 , sourceHeight=125 },
		{ name=2, x = 74, y = 0, width = 63, height = 118, sourceX=7, sourceY=5, sourceWidth=74 , sourceHeight=125 },
		{ name=3, x = 194, y = 124, width = 45, height = 117, sourceX=23, sourceY=6, sourceWidth=74 , sourceHeight=125 },
		{ name=4, x = 140, y = 124, width = 52, height = 123, sourceX=16, sourceY=1, sourceWidth=74 , sourceHeight=125 },
		{ name=5, x = 140, y = 0, width = 53, height = 122, sourceX=15, sourceY=0, sourceWidth=74 , sourceHeight=125 },
		{ name=6, x = 72, y = 123, width = 66, height = 112, sourceX=4, sourceY=0, sourceWidth=74 , sourceHeight=125 },
		{ name=7, x = 0, y = 0, width = 72, height = 121, sourceX=2, sourceY=0, sourceWidth=74 , sourceHeight=125 }
	},
	sheetContentWidth = 256,
	sheetContentHeight = 256
}

function M:getSheet()
    return self.sheetData;
end

return M