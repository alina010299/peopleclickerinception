local M = {}
M.sheetData = {
	frames = {
		{ name=death1_1, x = 244, y = 0, width = 46, height = 127, sourceX=157, sourceY=0, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_2, x = 0, y = 293, width = 86, height = 117, sourceX=112, sourceY=9, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_3, x = 0, y = 195, width = 125, height = 96, sourceX=69, sourceY=32, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_4, x = 0, y = 90, width = 172, height = 58, sourceX=15, sourceY=71, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_5, x = 0, y = 150, width = 171, height = 43, sourceX=10, sourceY=91, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_6, x = 0, y = 45, width = 177, height = 43, sourceX=4, sourceY=91, sourceWidth=203 , sourceHeight=138 },
		{ name=death1_7, x = 0, y = 0, width = 180, height = 43, sourceX=1, sourceY=91, sourceWidth=203 , sourceHeight=138 },
		{ name=run1, x = 162, y = 195, width = 70, height = 121, sourceX=0, sourceY=3, sourceWidth=74 , sourceHeight=125 },
		{ name=run2, x = 179, y = 45, width = 63, height = 118, sourceX=7, sourceY=5, sourceWidth=74 , sourceHeight=125 },
		{ name=run3, x = 285, y = 290, width = 45, height = 117, sourceX=23, sourceY=6, sourceWidth=74 , sourceHeight=125 },
		{ name=run4, x = 234, y = 165, width = 52, height = 123, sourceX=16, sourceY=1, sourceWidth=74 , sourceHeight=125 },
		{ name=run5, x = 230, y = 318, width = 53, height = 122, sourceX=15, sourceY=0, sourceWidth=74 , sourceHeight=125 },
		{ name=run6, x = 162, y = 318, width = 66, height = 112, sourceX=4, sourceY=0, sourceWidth=74 , sourceHeight=125 },
		{ name=run7, x = 88, y = 293, width = 72, height = 121, sourceX=2, sourceY=0, sourceWidth=74 , sourceHeight=125 }
	},
	sheetContentWidth = 512,
	sheetContentHeight = 512
}

function M:getSheet()
    return self.sheetData;
end
return M