local M = {}
M.sheetData = {
	frames = {
		{ name=Armature_win_0, x = 49, y = 258, width = 44, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 },
		{ name=Armature_win_1, x = 49, y = 129, width = 45, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 },
		{ name=Armature_win_2, x = 0, y = 0, width = 48, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 },
		{ name=Armature_win_3, x = 0, y = 129, width = 47, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 },
		{ name=Armature_win_4, x = 0, y = 258, width = 47, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 },
		{ name=Armature_win_5, x = 50, y = 0, width = 44, height = 127, sourceX=0, sourceY=0, sourceWidth=49 , sourceHeight=128 }
	},
	sheetContentWidth = 128,
	sheetContentHeight = 512
}
function M:getSheet()
    return self.sheetData;
end
return M