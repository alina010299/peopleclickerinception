-- include composer and widget libraries
local composer = require("composer")
local widget = require("widget");
local profile = require("util.profile")
-- create new scene for main menu
local scene = composer.newScene()
local sceneGroup

-- get display metricss
local W = display.contentWidth -- 1920
local H = display.contentHeight -- 1080

-- variable for skull heap image
local skulls

-- variables for menu buttons
local toNewGame
local toGame
local toMarket
local toOptions

-- unavailable option
local toSomeNewBehavior

-- 'button'(entire screen) for activation/deactivation main menu
local toActiveMenu
-- flashing label 'tap to continue'
local toActiveMenuText
-- main menu label 'People Clicker: Inception' 
local gameName

--- @autor sergeevgk
--- Redirect to scene 'map' (new game)
-- @param event
-- FIXME : integrate with 'map' scene  
local function gotoNewGame(event)
  if (event.phase == "ended") then
    composer.gotoScene( "cutscenes.OpeningCutScene" )
  end  -- if "ended"

end --gotoNewGame

--- @autor sergeevgk
--- Redirect to scene 'map' (continue game)
-- @param event
-- FIXME : integrate with 'map' scene  
local function gotoGame(event)
  --composer.gotoScene("/battle_field/SpawnButton")
 -- local composer = require( "composer" )
 
  -- Later...
  if (event.phase == "ended") then
    composer.gotoScene( "battle_field.battle_field" )
  end -- if "ended"

end   -- gotoGame

--- @autor sergeevgk
--- Redirect to scene 'market' (upgrades)
-- @param event
-- FIXME : integrate with 'market' scene  
local function gotoMarket()
  -- composer.gotoScene("market")
end -- gotoMarket

--- @autor sergeevgk
--- Redirect to scene 'options' (game settings)
-- @param event
-- FIXME : integrate with 'options' scene  
local function gotoOptions()
  -- composer.gotoScene("options")
end -- gotoOptions

-- @autor MCepulis
-- Redirect to scene 'choose profile'
-- @param event
local function gotoChangeProfile(event)
  profile.activateLogInDialog()
end -- ChangeProfile


--- @autor sergeevgk
--- Redirect to scene 'someNewScene' (coming soon)
-- @param event
-- FIXME : integrate with 'someNewScene' scene 
local function SomeNewBehavior()
  -- composer.gotoScene("someNewScene")
end -- SomeNewBehavior

--- @autor sergeevgk
--- Set menu buttons visible
local function setMenuOn()
  toNewGame:setFillColor(1,1,1,1)
  toNewGame:setLabel("New Game")
  toGame:setFillColor(1,1,1,1)
  toGame:setLabel("Continue")
  toMarket:setFillColor(1,1,1,1)
  toMarket:setLabel("Upgrade")
  toSomeNewBehavior:setFillColor(1,1,1,1)
  toSomeNewBehavior:setLabel("Coming Soon")
end -- setMenuOn

--- @autor sergeevgk
--- Set menu buttons invisible
local function setMenuOff()
  toNewGame:setFillColor(1,1,1,0)
  toNewGame:setLabel("")
  toGame:setFillColor(1,1,1,0)
  toGame:setLabel("")
  toMarket:setFillColor(1,1,1,0)
  toMarket:setLabel("")
  toSomeNewBehavior:setFillColor(1,1,1,0)
  toSomeNewBehavior:setLabel("")
end -- setMenuOff

-- flag value for active/inactive menu phase
local flag = true

--- @autor sergeevgk
--- Change menu state from active to inactive and reversed
--- NB! 'Options' button is available in each menu state 
local function changeStateMenu()
  if (flag == true) then
    setMenuOn()
    timer.pause(txtFlash)
    toActiveMenuText:setFillColor(1,1,1,0)
    sceneGroup:insert(1, skulls)
    sceneGroup:insert(1, toActiveMenu)  --inserting with @param '1' inserts to bottom of table
    flag = false
  else
    setMenuOff()
    timer.resume(txtFlash)
    sceneGroup:insert(toActiveMenuText)
    toActiveMenuText:setFillColor(28/255,28/255,28/255,1)
    flag = true
  end -- if else flag
end -- changeStateMenu

--variable for timer 'txtFlash'
local limit = 100
local delta = -3

--- @autor sergeevgk
--- Change text's fillColor.alpha parameter in according to timer (alpha range ~ 0.65)
-- @param textToFlash : TextObject containing some label. Connected with timer
function flashingText(textToFlash)
  limit = limit + delta
  if (limit < 10) then
    delta = 3
  elseif (limit > 75) then
    delta = -3
  end -- if elseif limit

  transition.to( textToFlash, {time=40, alpha=1-limit/130})
end -- flashingText

--- @autor sergeevgk
--- Check current system memory state and collects garbage
local function checkMemory()
   collectgarbage( "collect" )
   local memUsage_str = string.format( "MEMORY = %.3f KB", collectgarbage( "count" ) )
   --print( memUsage_str, "TEXTURE = "..(system.getInfo("textureMemoryUsed") / (1024 * 1024) ) )
end -- checkMemory



local function loadProfiles()

  if(not profile.init()) then profile.save() end

  if(not profile.getActive()) then
    print("Active user", profile.getActive())
    profile.activateLogInDialog()
  else
    print("Active user id", profile.getActive().id)
  end  -- not getActive


end -- loadProfiles





--- @autor sergeevgk
--- Create scene setting sceneGroup
-- @param event
function scene:create(event)
  -- ERROR review akzhukov
  -- этот таймер нужен вообще? это хорошо или плохо? 
  -- ANSWER MisterProper9000 Функция нужна скорее для дебага, чтоб выявить утечки памяти (если расскомененть вывод памяти внутри функции)
  timer.performWithDelay( 500, checkMemory, 0 )
  sceneGroup = self.view
end -- create

--- @autor sergeevgk
--- Create `visible` objects to insert into sceneGroup (buttons, text)
-- @param event
function scene:show(event)
  local sceneGroup = self.view
  local phase = event.phase
  local textSize = 45
  local buttonWidth = W/3
  local buttonHeight = H/7
  local buttonX = display.contentCenterX
  
  if(phase == "will") then
    print(H, W); 
    

    toNewGame = widget.newButton(
      {
        label = "New Game",
        labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
        fontSize = textSize,
        font = 'fonts/Shumi.otf',
        x = buttonX,
        y = H * 0.25,
        width = buttonWidth,
        height = buttonHeight,
        defaultFile = "menu_includes/MenuRect.png",
        onEvent = gotoNewGame,
      }
    )
  sceneGroup:insert(toNewGame)
  toGame = widget.newButton(
      {
        label = "Continue",
        labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
        fontSize = textSize,
        font = 'fonts/Shumi.otf',
        x = buttonX,
        y = H * 0.45,
        width = buttonWidth,
        height = buttonHeight,
        defaultFile = "menu_includes/MenuRect.png",
        onEvent = gotoGame, -- onEvent is necessary for using event.phase in gotoGame 
      }
    )
  sceneGroup:insert(toGame)
  toMarket = widget.newButton(
      {
        label = "Upgrades",
        labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
        fontSize = textSize,
        font = 'fonts/Shumi.otf',
        x = buttonX,
        y = H * 0.65,
        width = buttonWidth,
        height = buttonHeight,
        defaultFile = "menu_includes/MenuRect.png",
        onPress = gotoMarket,
      }
    )
  sceneGroup:insert(toMarket)
  
  toSomeNewBehavior = widget.newButton(
      {
        label = "Coming Soon",
        labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
        fontSize = textSize,
        font = 'fonts/Shumi.otf',
        x = buttonX,
        y = H * 0.85,
        width = buttonWidth,
        height = buttonHeight,
        defaultFile = "menu_includes/MenuRect.png",
        onPress = SomeNewBehavior,
      }
    )
  sceneGroup:insert(toSomeNewBehavior)
  toActiveMenu = widget.newButton(
    {
      x = W/2,
      y = H/2,
      width = W,
      height = H,
      defaultFile = "menu_includes/MenuBack.png",
      onPress = changeStateMenu,
    }
  )
  sceneGroup:insert(toActiveMenu)

  toOptions = widget.newButton(
    {
      x = W * 0.92,
      y = 0.09 * H,
      width = 100,
      height = 100,
      defaultFile = "menu_includes/settings.png",
      onPress = gotoOptions
    }
  )
  sceneGroup:insert(toOptions)

  toChangeProfile = widget.newButton(
    {
      x = W * 0.08,
      y = 0.09 * H,
      width = 100,
      height = 100,
      defaultFile = "menu_includes/logout.png",
      onPress = gotoChangeProfile
    }
  )
  sceneGroup:insert(toChangeProfile)



  skulls =  display.newImageRect("menu_includes/SkullsComplete.png", 2*H/3*910/445, 2*H/3)
  skulls.x = display.contentCenterX
  skulls.y = display.contentCenterY
  sceneGroup:insert(skulls)

  local gameNameOptions = {
    parent = sceneGroup,
    text = "People Clicker: Inception", 
    x = W*0.5, 
    y = H * 0.1, 
    font = 'fonts/Shumi.otf', 
    fontSize = 100,
    align = "center"
  }
  gameName = display.newText(gameNameOptions)
  gameName:setFillColor(112/255, 39/255, 39/255)
  --sceneGroup:insert(gameName)
  toActiveMenuText = display.newText(toActiveMenu, "Tap to play...", W * 0.5, H * 0.9, 'fonts/Shumi.otf', 50)
  toActiveMenuText:setFillColor(28/255,28/255,28/255)
  txtFlash = timer.performWithDelay(60, function() flashingText(toActiveMenuText) end, 0)

  loadProfiles()
  elseif (phase == "did") then

  end --if elseif phase
end -- show

--- @autor sergeevgk
--- Delete objects inserted into sceneObject
-- @param event
function scene:hide(event)
  local sceneGroup = self.view
  local phase = event.phase

  if(phase == "will") then
    print("menu hide will")

  elseif (phase == "did") then
  toNewGame:removeSelf()
  toNewGame = nil
  toGame:removeSelf()
  toGame = nil
  toMarket:removeSelf()
  toMarket = nil
  toOptions:removeSelf()
  toOptions = nil
  toChangeProfile:removeSelf()
  toChangeProfile = nil
  toSomeNewBehavior:removeSelf()
  toSomeNewBehavior = nil
  toActiveMenu:removeSelf()
  toActiveMenu = nil
  toActiveMenuText:removeSelf()
  toActiveMenuText = nil
  gameName:removeSelf()
  gameName = nil
  skulls:removeSelf()
  skulls = nil
  end -- if elseif phase
end -- hide

--- @autor sergeevgk
--- Destroys scene in the end of scene's life cycle
-- @param event
function scene:destroy(event)
  local sceneGroup = self.view
  checkMemory()
end --destroy

-- Add listeners for scene functions

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
return scene