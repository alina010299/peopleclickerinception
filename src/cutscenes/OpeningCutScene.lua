-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 --- @autor MisterProper9000

local composer = require("composer")
local loadsave = require("util.loadsave")
local scene = composer.newScene()

local soundTable = { -- таблица использующихся звуков в модуле

  -- logoTyping = audio.loadStream(""),
  -- nameAppear = audio.loadStream(""),

}

-- вычисление размера таблицы
function tableLength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end



local dialog = loadsave.loadTable("speech/soldiersDialogEng.json",system.ResourceDirectory)

local W = display.contentWidth  -- ширина дисплея
local H = display.contentHeight -- высота дисплея

local subTitlesRect -- прямоугольник субтитров
local nameRect -- прямоугольник имени

local subTitlesObj -- субтитры
local nameObj -- имя

local backGroup -- группа объектов катсцены
local backImg -- картинка заднего фона


local text = " " -- текст для субтитров

local Tommy = "Tommy"
local Larry = "Larry"
local line = 0
local linesNum = tableLength(dialog)
local name
local length = 0
local pause = 0

local typewriterTimer -- таймер для вывода субитров

local subTitlesFunction = function(event) -- функция таймера

  if(line == 2*linesNum and pause == 0) then --временное условие окончания катсцены
    composer.gotoScene( "battle_field.battle_field" ) 
  end 

  if(line <= linesNum and pause == 0) then -- если пришло время обновить субтитры
    
    subTitlesObj.text = string.sub(text, 1, length) -- прибавляем символ к строке на экране
    nameObj.text = name -- имя не меняется
    nameRect.alpha = 1
    subTitlesRect.alpha = 1
    if(length == string.len(text)) then -- если вывели текущую фразу субтитров полностью
      
      line = line + 1 -- переходим к следующей
      name = (line % 2 == 0) and Larry or Tommy -- какой-то странный хардкод смены имён, но пока слабо вижу как обобщить это
      text = dialog[name.." "..line] -- выбираем из таблицы следующую реплику
      length = 1 -- длина строки снова равна 1
      pause = 30 -- делаем паузу, чтоб реплики не заменялись мгновенно
    end
    length = length + 1
  end

  if(line > linesNum and pause == 0) then -- если всё показали, тогда выставляем условия выхода
    nameRect.alpha = 0
    subTitlesRect.alpha = 0
    subTitlesObj.text=""
    nameObj.text = ""
    line = 2*linesNum -- для временного условия окончания катсцены
    pause = 50 -- для временного условия окончания катсцены
  end

  if(pause > 0) then pause = pause - 1 end -- кулдаун между репликами
  
end -- end subTitlesFunction

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
    
 
    local sceneGroup = self.view
    
    -- Code here runs when the scene is first created but has not yet appeared on screen

 
end -- end create

-- show()
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase
 
  if ( phase == "will" ) then
    -- Code here runs when the scene is still off screen (but is about to come on screen)
    
    local text = " " -- текст для субтитров

    subTitlesRect = display.newRect(W/2, H, W, 160)

    subTitlesRect.anchorY = 1
    subTitlesRect:setStrokeColor( 0,0,0 )
    subTitlesRect.strokeWidth = 8
    nameRect = display.newRect(160, H-120, W/5, 80)
    nameRect.anchorY = 1
    nameRect.anchorX = 0
    nameRect:setStrokeColor( 0,0,0 )
    nameRect.strokeWidth = 4
    nameRect.alpha = 0

    subTitlesObj = display.newText({ -- настройки текста субтитров
      text = "",
      font = "fonts/Tavolga.otf",
      fontSize = 40,
      align = "left",
      x = 180,
      y = H - 60,
      anchorY = 1
    })
    subTitlesObj:setFillColor(0,0,0,1)
    subTitlesObj.anchorX = 0
    nameObj = display.newText({ -- настройки текста имени
      text = "",
      font = "fonts/Tavolga.otf",
      fontSize = 40,
      align = "left",
      x = 200,
      y = H - 160,
      anchorY = 1
    })
    nameObj:setFillColor(0,0,0,1)
    nameObj.anchorX = 0

    Tommy = "Tommy"
    Larry = "Larry"
    line = 0
    length = 0
    pause = 0
    subTitlesRect.alpha = 0
    nameRect.alpha = 0

    backImg = display.newImageRect(sceneGroup, "cutscenes/backs/soldierback.png", W,W*1544/1280)

    BackGroup = display.newGroup()
    BackGroup:insert(backImg)

    -- positioning back group
    BackGroup.x = W/2
    BackGroup.y = math.ceil(W*1544/1280/2)
    print(H,BackGroup.y)
    sceneGroup:insert(BackGroup)

    
    sceneGroup:insert(subTitlesRect)
    sceneGroup:insert(subTitlesObj)
    sceneGroup:insert(nameRect)
    sceneGroup:insert(nameObj)

    Runtime:addEventListener("enterFrame", update) -- выполняем функцию update каждый кадр
  elseif ( phase == "did" ) then
    -- Code here runs when the scene is entirely on screen
    typewriterTimer = timer.performWithDelay(30, subTitlesFunction, 0) -- запускаем функцию subTitlesFunction каждые 30 мс
  end -- end phase if
  
end -- end show


local freeze = 120 -- начальная задержка перед смещением камеры вниз

function  update( event )
  if(freeze > 0) then
    freeze = freeze - 1
    return
  end
  if(BackGroup.y > H+10-math.floor(W*1544/1280/2)) then BackGroup.y = BackGroup.y - 2 end
end -- end update

 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
      -- Code here runs when the scene is on screen (but is about to go off screen)
      print("cut scene hide will")

      -- всегда удаляем листенеры отовсюду
      Runtime:removeEventListener("enterFrame", update)
      timer.cancel(typewriterTimer)
      
      display.remove(nameRect)
      display.remove(nameObj)
      display.remove(subTitlesRect)
      display.remove(subTitlesObj)
      display.remove(BackGroup)
      BackGroup = nil
      print("cut scene hide will")
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
      print("cut scene hide did")
    end -- end phase if
    

end -- end hide
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
    
end -- end destroy
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene