
local composer = require( "composer" )
 
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local soundTable = { -- таблица использующихся звуков в модуле

  logoTyping = audio.loadStream("audio/keyboardtyping.wav"),
  nameAppear = audio.loadStream("audio/nameAppear.wav"),

}


local W = display.contentWidth  -- ширина дисплея
local H = display.contentHeight -- высота дисплея

 
 
 local LogoObj = display.newText({ -- настройки текста логотипа
   text = "",
   font = "fonts/BRUX.ttf",
   fontSize = 100,
   x = W/2,
   y = H/2,
})

local NameObj = display.newText({ -- настройки текста названия игры
   text = "",
   font = "fonts/Shumi.otf",
   fontSize = 140,
   x = W/2,
   y = H/2,
})
NameObj:setFillColor(112/255, 39/255, 39/255) -- цвет названия игры тёмно-красный

local gameName = "People Clicker. Inception"  -- текст названия игры
local logoTxt = "mDaush studio presents..." -- текст логотипа
local gameNameAlpha = 0  -- изначально название игры полностью прозрачно
local logoTyping = audio.play(soundTable["logoTyping"], {channel = 1}) -- начинаем проигрывать звук набора текста в первом канале
local nameAppear = audio.play(soundTable["nameAppear"], {channel = 2}) -- начинаем проигрывать звук появления названия игры во втором канале (если запускать из функции, то почему-то не работает)
audio.pause( 2 ) -- и сразу ставим на паузу второй канал, потому что название появляется после логотипа
audio.pause( 1 ) -- на паузу надо ставить и первый канал, иначе дорожка заиграет во время показа лого короны!
audio.setVolume(0.8, {channel = 2}) -- убавим громкость, а то чёт громко
local typewriterFunction = function(event) -- функция, которая обеспечит красоту в лого и названии
   if(event.count == 1) then
   	   audio.resume( 1 )
   
   elseif(event.count < string.len(logoTxt) + 2) then -- сначала просто набираем текст логотипа
   	  LogoObj.text = string.sub(logoTxt, 1, event.count)

   elseif (event.count == string.len(logoTxt)) then -- когда весь текст логотипа набран, останавливаем звук набора текста (логично, чё)
      audio.stop(logoTyping) -- останавливаем дорожку
      logoTyping = nil -- очищаем дорожку
      

   elseif (event.count == string.len(logoTxt) + 2) then -- через два вызова timer спустя окончания набора текста убираем текст логотипа и проигрываем звук появления текста
   	  display.remove(LogoObj) -- убираем текст логотипа
   	  LogoObj = nil -- зануляем его
   	  audio.resume( 2 ) -- запускаем дорожку во втором канале, который поставили на паузу в самом начале

   	  
   elseif (event.count < string.len(logoTxt) + 3 + string.len(gameName)) then -- в оставшиеся итерации timer'а до последней итерации появляется текст названия игры
   	  NameObj.text = string.sub(gameName, 1, event.count) -- появление текста названия игры
   	  NameObj:setFillColor(112/255, 39/255, 39/255, gameNameAlpha) -- установка прозрачности
   	  gameNameAlpha = gameNameAlpha + 0.1 -- текст названия игры становится всё более непрозрачным (но рывками, что есть стильно)
   
   elseif (event.count == string.len(logoTxt) + 3 + string.len(gameName)) then -- на последней итерации timer'а надо всё почитстить за собой и перейти на следующую сцену (в меню) Чистить можно всё, ибо в процессе игры лого только в начале показывается
   	  display.remove(NameObj) -- удаляем текст названия игры
   	  NameObj = nil -- зануляем его
   	  audio.stop(nameAppear) -- останавливаем дорожку звука появления текста названия игры
   	  logoTyping = nil -- очищаем дорожку
   	  -- удаление таблицы звуков: 
   	  audio.dispose(soundTable["logoTyping"]) 
   	  soundTable["logoTyping"] = nil
   	  audio.dispose(soundTable["nameAppear"])
   	  soundTable["nameAppear"] = nil
   	  audio.setVolume(1, {channel = 2}) -- возращаем громкость второго канала обратно в норму

   	  composer.gotoScene("menu", {700, effect = "zoomInOut"}) -- переходим к следующей сцене
   end -- окончание проверки номера итерации

end -- окончание функции typewriterFunction

local typewriterTimer = timer.performWithDelay(150, typewriterFunction, string.len(logoTxt) + 3 + string.len(gameName)) -- создадим таймер и будем вызывать typewriterFunction string.len(logoTxt) + 3 + string.len(gameName) раз
 

return scene